module debouncer (
  input logic clock,
  input logic in,
  output logic out
);
  logic in_buffer, in_sync_buffer;

  logic input_signal, signal, output_signal;

  logic [3:0] counter;

  assign out = output_signal;

  always_ff @ (posedge clock) begin
    input_signal <= in_sync_buffer;

    signal <= input_signal;

    counter <= (signal == input_signal) ? counter + 1 : 1;

    if (counter [3]) begin
      output_signal <= signal;
    end
  end

  always_ff @ (posedge clock) begin
    in_buffer <= in;

    in_sync_buffer <= in_buffer;
  end
endmodule
