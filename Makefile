BUILDDIR = build

VERILOG = top.sv \
	  debouncer.sv \
	  asynchronous_state_machine.sv \

LPF = orange-crab-0.2.lpf

PACKAGE=CSFBGA285

NEXTPNR_FLAGS=--85k --speed 8 --freq 48 --ignore-loops

compile: $(BUILDDIR)/toplevel.bit

prog: $(BUILDDIR)/toplevel.bit
	ecpprog -S $^

$(BUILDDIR)/toplevel.json: $(VERILOG)
	mkdir -p $(BUILDDIR)
	yosys -p "synth_ecp5 -noflatten -asyncprld -top top -json $@" $^

$(BUILDDIR)/%.config: $(BUILDDIR)/toplevel.json $(LPF)
	nextpnr-ecp5 --json $< --lpf $(LPF) --textcfg $@.tmp $(NEXTPNR_FLAGS) --package $(PACKAGE)
	mv -f $@.tmp $@

$(BUILDDIR)/toplevel.bit: $(BUILDDIR)/toplevel.config
	ecppack --compress  --freq 62.0 $^ $@

clean:
	rm -rf ${BUILDDIR}

.SECONDARY:
.PHONY: compile clean prog


