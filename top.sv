module top (
  input logic ext_clk,
  input logic ext_rst_n,
  input logic usr_btn,
  output logic pin_gpio_9,
  output logic pin_gpio_10,
  output logic led0_r,
  output logic led0_g,
  output logic led0_b
);
  wire clock = ext_clk;

  wire push_button = ~usr_btn;

  wire debug_output = pin_gpio_9;

  logic debouncer_output;

  logic out;

  assign led0_r = 1;

  assign led0_g = out ? 0 : 1;

  assign led0_b = 1;

  assign debug_output = usr_btn;

  assign pin_gpio_10 = out;

  debouncer debouncer_instance (
    .clock (clock),
    .in (usr_btn),
    .out (debouncer_output)
  );

  asynchronous_state_machine asm_instance (
    .in (debouncer_output),
    .out (out)
  );
endmodule
