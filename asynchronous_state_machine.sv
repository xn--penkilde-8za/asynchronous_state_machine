module asynchronous_state_machine (
  input logic in,
  output logic out
);
  logic state;

  logic state_next;

  always_latch begin
    if (in) begin
      state <= state_next;
    end
  end

  /**
   * the `always_latch` above should have produced the TRELLIS_FF below
   */
  // TRELLIS_FF # (
  //   .SRMODE ("ASYNC"),
  //   .LSRMODE ("PRLD"),
  //   .LSRMUX ("INV")
  // ) TRELLIS_FF_instance (
  //   .CLK (0),
  //   .LSR (in),
  //   .CE (1),
  //   .DI (state_next),
  //   .M (state_next),
  //   .Q (state)
  // );

  always_comb begin
    unique case (state)
      1'b0: begin
        out = 1'b0;

        state_next = 1'b1;
      end

      1'b1: begin
        out = 1'b1;

        state_next = 'b0;
      end

      default: begin
        out = 1'b0;

        state_next = 'b0;
      end
    endcase
  end
endmodule
